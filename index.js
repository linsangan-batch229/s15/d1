// JS Comments ctrl+/
// They are  important, acts as a guide for the programmer
// Comments are disregarded no matter how long it may be.

//multi-line comments ctrl+shift+/


// STATEMENTS
// Programming instructions that we tell the computer to perform.
// Statements usually ends with semicolo (;)

// SYNTAX
// it is the set of rules that describes how statements must be constructed



// alert("Hello Again");

//this is a statement with a correct syntax
console.log("hello word");


//JS is a loose type PL
console. log ( "hello word" );

//with this
console.
log
(
"qweq"

	)


// SECTION variables

//declaring variables 
//SYNTAX - let/const variableName;

// "let" is a keyword that is usually used to declare a varible

let myVariale;

console.log(myVariale);


// = sign stands for initialization

//guideles for declaring a var
// 1. use let
// 2. use lowercase for first letter
// 3. for constant variable we are using const keyword
// 4. variables names should be comprehensive or descriptive
// 5. do not use spaces on the declared variables

//different casing style
// cammelCase - firstName
// snake case - first_Name
// kebab case - first-Name

let firstName = "ronel";

let pokemon = 25000;



// Declaring and initializing variables
// Syntax -> let/const variableName = value;

let productName = "desktop computer";
console.log(productName);


let productPrice = 150;
console.log(productPrice);


//re-assigning a value to a variable
productPrice = 25;
console.log(productPrice);

let friend = "kate";

let supplier;

// let vs cons
// let variables - values can be changed, we can re-assign new values
// const variables - values cannot be changed,

// This is a global variable
let outerVar = "hi" 

// this is a local variable
{
	let innerVariable = "hello"
}


// multiple variable declared may be declared in //one line.
// convinient and it is easier to read the code



let productCode = "DC017", ProductBrand = "Dell";
console.log(productCode, ProductBrand);

// Strings - series of chars
//concantenating stirng using "+""
// 

//escapre character (\)
//  "\n" refers to creating a new line between text

let mailAdd = "Metro Manila \nwewe";
console.log(mailAdd);


// backslah 
let msg = "J employe";
console.log(msg);

msg = "John\'s"

// numbers
// Integers or whole numbers

let headcount = 26;
console.log(headcount);

// Decial or fractions
let grade = 1.1;
console.log(grade);

//exponential notation
let grade1 = 2e10;

//combining text and number
console.log("john " + grade1);

//Boolean values




//Arrays
// use to store multiple value with the same data type

//syntax - > let array = [element1, element2];

let grade12 = [98, 92, 12];

// Objects
// Hold properties that describes the variable
//Syntax - let/const objectName = {propertyA: value, propertyB: value};

let person = {
	fullname: "we",
	age: 35,
	contact: ["!@3", "wew"],
	address: {
		house : "25423",
		city: "Manila"
	}
}


let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};

console.log(myGrades);

//checking of data type,
console.log(typeof myGrades);
console.log(typeof person);

const anime =[
	"one piece", "one punch man",
	"Atk on titan"];

// anime = ["kimtsu no yaiba"];

anime[0] = ["kimetsu"];
console.log(anime);

// null vs undefined
// null when variable has 0 or empty value
// undefined when a variable has no value

let spouse = 0; //null/empty
let friend; //undefined